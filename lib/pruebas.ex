defmodule Pruebas do
  def enum(path) do
    files =
      for file <- File.ls!(path) do
        "#{path}/#{file}"
      end
    number_of_documents = length(files)
    files
    |> Enum.with_index()
    |> Enum.flat_map(fn {path, index} ->
      File.stream!(path)
      |> Enum.flat_map(fn line ->
        Regex.scan(~r/\w+/u, line)
        |> Enum.map(fn [word] -> {index, String.downcase(word)} end)
      end)
      |> fn list ->
        len = length(list)
        Enum.map(list, fn {index, word} -> {index, word, len} end)
      end.()
    end)
    |> Enum.frequencies()
    |> Enum.group_by(fn {{_docid, word, _docsize}, _freq} -> word end, fn {{docid, word, docsize}, freq} -> {word, docid, freq/docsize} end)
    |> Enum.flat_map(fn {_word, list} ->
      number_of_documents_containing_word = length(list)
      Enum.map(list, fn {word, docid, tf} ->
        idf = :math.log10( number_of_documents / number_of_documents_containing_word )
        {word, docid, tf, idf, tf*idf}
      end)
    end)
    |> Enum.to_list()
  end
end
